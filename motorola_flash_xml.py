import xml.etree.cElementTree as ET
import hashlib
import os.path as path

def parse_servicefile(file_name): # currently unused
    tree = ET.ElementTree(file=file_name)
    flashing = tree.getroot()
    # Parse header
    header = flashing[0]
    header_dict = {}
    for data in header:
        name = data.tag
        item = list(data.attrib.values())
        if len(item) >= 1:
            header_dict[name] = item[0]
    
    # Parse flashing steps
    steps = flashing[1]
    steps_list = []
    for step in steps:
        steps_list.append(step.attrib)
    
    return header_dict, steps_list

def generate_script(steps):
    outlines = []
    errors = []
    for step in steps:
        newline_a = 'fastboot'
        newline_b = None
        validated = True
        if 'operation' in step:
            newline_a = newline_a + ' ' + step['operation']
        if 'partition' in step:
            if step['partition'] != 'super':
                newline_b = newline_a + ' ' + step['partition'] + '_b'
                newline_a = newline_a + ' ' + step['partition'] + '_a'
            else:
                newline_a = newline_a + ' ' + step['partition']
        if 'var' in step:
            newline_a = newline_a + ' ' + step['var']
        if 'filename' in step:
            if hashlib.md5(open(step['filename'],'rb').read()).hexdigest() == step['MD5']:       
                newline_a = newline_a + ' ' + step['filename']
                if newline_b is not None:
                    newline_b = newline_b + ' ' + step['filename']
            else:
                validated = False
                errors.append(step['filename']+' failed to validate, it will not be flashed.')
        
        if validated:
            outlines.append(newline_a)
            if newline_b is not None:
                outlines.append(newline_b)
    outlines.append('fastboot reboot')
    outlines.append('echo ""')
    outlines.append('echo "Your phone will now reboot."')
    return outlines, errors

def write_to_file(filename, outlines):
    outfile = open(filename, 'w')
    for line in outlines:
        outfile.write(line+'\n')
    outfile.close()

if __name__ == "__main__":
    # Parse the service.xml file
    if path.isfile('servicefile.xml'):
        info, steps = parse_servicefile('servicefile.xml')
    else:
        print('servicefile.xml is missing, exiting.')
        exit()
    
    # Print info about the ROM
    print('Generating fastboot flash script for',info['phone_model'])
    print('Target software version:', info['software_version'])
    print()
    
    # Generate the script
    outlines, errors = generate_script(steps)
    
    # Write the script to files
    write_to_file('flash_all.bat',outlines)
    
    # Print report
    if len(errors) >= 1:
        print('The script was created, but the following errors occured:')
        for error in errors:
            print('-',error)
    else:
        print('The script was created as \'flash_all.bat\', it can be run to flash the phone.')
